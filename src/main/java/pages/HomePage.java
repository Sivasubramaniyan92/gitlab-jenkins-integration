package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import basePackage.Base;

public class HomePage extends Base{
	
	//Page Factory - initialize WebElements with driver instance
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	// Object Repository
	@FindBy(xpath = "//button[@class='_2AkmmA _29YdH8']")
	WebElement loginPopup_Button_xpath;
	
	/*
	 * This method close login pop-up 
	 * without giving credentials
	 * 
	 */
	public void closeLoginPopub()
	{
		try {
		loginPopup_Button_xpath.click();
		}catch(Exception E)
		{
			E.printStackTrace();
		}
	}
	
/*
 * This Method calls screenshot method in base class
 * to take screenshot of the homePage
 *  
 */
	public void homePageScreenshot()
	{
		takeScreenShot("homePage");
	}
	
	

}
