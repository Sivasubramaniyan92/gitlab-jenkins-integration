package basePackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.io.Files;

public class Base {
	
	public static WebDriver driver;
	
	public static Properties prop;

	public Base(){
		try {
			// property initiailize
			prop = new Properties();
			FileInputStream ip = new FileInputStream("src\\main\\java\\utility\\property.properties");
		//	System.out.println(System.getProperty("user.dir"));
			prop.load(ip);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	/*
	 * This method will launch browser
	 * by using Property file URL keyword.
	 * Implicit Wait = 20 Seconds
	 * 
	 */
	public void initializeBrowser()
	{
		
		System.setProperty("webdriver.chrome.driver", "src\\main\\java\\chromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(prop.getProperty("URL"));
			
	}
	public void takeScreenShot(String name)
	{
		TakesScreenshot ts = (TakesScreenshot)driver;
		File scrFile = ts.getScreenshotAs(OutputType.FILE);
		try {
			Files.copy(scrFile, new File("Screenshot\\"+name+".jpeg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
