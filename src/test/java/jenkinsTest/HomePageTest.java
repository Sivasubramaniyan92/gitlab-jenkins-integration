package jenkinsTest;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import basePackage.Base;
import pages.HomePage;

public class HomePageTest extends Base {

	HomePage homePage;
	
	@BeforeMethod
	public void launchBrowser()
	{
		// initialize browser
		initializeBrowser();
	}
	
	@Test
	public void takeHomePageScreenshot(){
		homePage = new HomePage();
		homePage.closeLoginPopub();
		homePage.homePageScreenshot();
		
	}
	@Test
	public void failTestCase()
	{
		Assert.assertTrue(false);
	}
	
	@Test
	public void passTestCase()
	{
		Assert.assertTrue(true);
	}
	
	@AfterMethod
	public void tearDown()
	{
		driver.close();
	}
	
}
